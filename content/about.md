Original art from Dark Sun: Wake of the Ravager (1994):

{{< figure src="/img/about/darksun_pfp.png" >}}
{{< figure src="/img/about/darksun.png" >}}

Custom art by @norshiex:

{{< figure
src="/img/about/cyberpunk.png" 
caption="Cyberpunk Yuan-Ti"
height=512
>}}
