---
title: "Best of TheoryOverflow"
date: 2012-01-15
tags: ["theory"]
---
The StackExchange site for theoretical CS discussion has been around for well
over a year now, and it's been host to a number of interesting threads. My
favorites include:

* [What's new in purely functional data structures since Okasaki?](http://cstheory.stackexchange.com/questions/1539/whats-new-in-purely-functional-data-structures-since-okasaki)
* [Is Magic: the Gathering Turing complete?](http://cstheory.stackexchange.com/questions/2052/is-magic-the-gathering-turing-complete)
* [Can the cost of GC be neglected when analyzing the running time of worst-case data structures specified in a garbage-collected programming language?](http://cstheory.stackexchange.com/questions/2720/can-the-cost-of-gc-be-neglected-when-analyzing-the-running-time-of-worst-case-da)
* [What would you advise someone who wants to do research as a hobby?](http://cstheory.stackexchange.com/questions/2966/what-would-you-advise-someone-who-wants-to-do-research-as-a-hobby)
* [Common false beliefs in theoretical computer science](http://cstheory.stackexchange.com/questions/3616/common-false-beliefs-in-theoretical-computer-science)
* [How do I choose a functional dictionary data structure?](http://cstheory.stackexchange.com/questions/4007/how-do-i-choose-a-functional-dictionary-data-structure)
* [Powerful Algorithms too complex to implement](http://cstheory.stackexchange.com/questions/4491/powerful-algorithms-too-complex-to-implement)
* [How do you get a “Physical Intuition” for results in TCS?](http://cstheory.stackexchange.com/questions/4806/how-do-you-get-a-physical-intuition-for-results-in-tcs)
* [Axioms necessary for theoretical computer science](http://cstheory.stackexchange.com/questions/4816/axioms-necessary-for-theoretical-computer-science)
* [Succinct data structures survey?](http://cstheory.stackexchange.com/questions/5010/succinct-data-structures-survey)
* [Super Mario Galaxy problem](http://cstheory.stackexchange.com/questions/6356/super-mario-galaxy-problem)
* [Casual tours around proofs](http://cstheory.stackexchange.com/questions/8869/casual-tours-around-proofs)
* [Original proofs generated on the parent site](http://meta.cstheory.stackexchange.com/questions/784/original-proofs-generated-on-the-parent-site)

{{< figure src="/img/maximus.png" class="alignleft" >}}
